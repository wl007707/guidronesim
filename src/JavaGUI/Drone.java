package JavaGUI;

import java.util.Random;

/**
 * @author Ryan-
 *
 */
public class Drone extends Items {

    double velocity = 2; //speed of object
    double angle; //Angle for direction of travel
    int droneID; // Unique ID of drone
    static int startID = 0; //Starting ID

    /**
     * @param x Drone's X value
     * @param y Drone's Y value
     */
    public Drone(double x, double y) {
        super(x, y);
        size = 3; //All drones are size 3
        droneID = startID++; //increments so no drone has same id
        type = 'b';
        Random r = new Random(System.currentTimeMillis()); //Using sytem time as a seed to make random number more random
        angle = r.nextFloat() * 2 * Math.PI;
    }

    public double getX() {
        return posX;
    }

    public double getY() {
        return posY;
    }

    public void setID(int ID) {
        droneID = ID;
    }

    /**
     * @param v Velocity of drone
     * @param rad Angle the drone faces
     */
    public void setDirection(double v, double rad) {
        velocity = v;
        angle = rad;
    }


    public String toString() { //What displays in the list on the right side of the application
        return "Drone " + droneID + "\t | (" + String.format("%.1f", posX) + "," + String.format("%.1f", posY) + ")\t | Angle: " + String.format("%.1f", angle) + "\t";
    }

    /**
     * @param m The drone you're trying to move
     */
    public void tryToMove(DroneArena m) {
        double nextX = posX + velocity * Math.cos(angle); //takes drone's current angle and mosve in straight line
        double nextY = posY + velocity * Math.sin(angle);
        Random r = new Random();
        double randomAngle = 360 * r.nextDouble();

        if (m.canMoveHere(nextX, nextY, this)) {
            posX = nextX; //If space is avaliable, move the drone there
            posY = nextY;
        } else {
            angle += Math.toRadians(randomAngle);// If space is occupied/invalid, choose a new random direction
            nextX = posX + velocity * Math.cos(angle);
            nextY = posY + velocity * Math.sin(angle);
            posX = nextX;
            posY = nextY;
        }

    }

    /**
     * @param x X value to check
     * @param y Y value to check
     * @return Return boolean if it's touching
     */
    public boolean isTouching(double x, double y) {
        int radius = 20;
        if ((posY - y < radius && posY - y > -radius) && (posX - x < radius && posX - x > -radius)) {
            return true;
        }
        return false;
    }

    /**
     * @return Formatted string of information to save about the drones
     */
    public String saveDrone() {
        String saveData = "";
        saveData += type + "\n"; //Type of item stored by their colour. This makes it easier to draw later when loading
        saveData += droneID + "\n";
        saveData += posX + "\n"; //All these values are fairly self explanatory
        saveData += posY + "\n";
        saveData += velocity + "\n";
        saveData += angle;

        return saveData;
    }

}