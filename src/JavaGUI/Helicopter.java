package JavaGUI;

/**
 * @author Ryan-
 *
 */
public class Helicopter extends Drone {

    /**
     * @param x X position of Helicopter
     * @param y Y position of Helicopter
     */
    public Helicopter(double x, double y) {
        super(x, y);
        velocity = 0.5;
        size = 10;
        type = 'r';
    }

    public boolean isHere(double xVal, double yVal) {
        double upper = yVal - size - size * 2; //Checks the outermost limits of an item
        double lower = yVal + size + size * 2;
        double leftmost = xVal - size - size * 2;
        double rightmost = xVal + size + size * 2;

        if ((posX <= rightmost && posX >= leftmost) && (posY <= upper && posY >= lower)) {
            return true;
        }
        return false;
    }

}