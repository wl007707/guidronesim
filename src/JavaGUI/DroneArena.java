package JavaGUI;

//Require modules
import java.util.ArrayList;
import java.util.Random;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.swing.JFileChooser;
import java.io.File;
import java.io.IOException;

/**
 * @author Ryan-
 *
 */
public class DroneArena {
	private int arenaSizeX;
	private int arenaSizeY;
	private ArrayList<Drone> droneList = new ArrayList<Drone>();					//Lists for each of the objects
	private ArrayList<Obstacle> obstacleList = new ArrayList<Obstacle>();
	private ArrayList<Helicopter> helicopterList = new ArrayList<Helicopter>();
	private Random randomGenerator;
	
	/**
	 * @param x X value size of Arena
	 * @param y Y value size of Arena
	 */
	public DroneArena(int x, int y){
		arenaSizeX= x;
		arenaSizeY= y ;
		randomGenerator = new Random();
	}
	
	/**
	 * @param i Loop through each drone
	 */
	public void showDrone(CanvasGUI i){ //Draws the item onto the canvas
		for(Drone d: droneList){
			d.drawCircle(i);
		}
		for(Obstacle O: obstacleList){
			O.drawCircle(i);
		}
		for(Helicopter H: helicopterList){
			H.drawCircle(i);
		}
	}
	
	/**
	 * @param type The type of object you want added
	 */
	public void addDrone(char type){
		double xValue = randomGenerator.nextInt(arenaSizeX);
		double yValue = randomGenerator.nextInt(arenaSizeY);
		boolean collision = true;
		
		while(collision){
			if(getItemAt(xValue, yValue) == null){
				collision = false;
			}
			xValue = randomGenerator.nextInt(arenaSizeX);
			yValue = randomGenerator.nextInt(arenaSizeY);
		}
		if(type == 'd'){ //drone
			Drone newItem = new Drone(xValue,yValue);
			droneList.add(newItem);
		}
		if(type == 'o'){ //Obstacle
			Obstacle newItem = new Obstacle(xValue,yValue);
			obstacleList.add(newItem);
		}
		if(type == 'H'){ //Helicopter
			Helicopter newItem = new Helicopter(xValue,yValue);
			helicopterList.add(newItem);
		}
	}

	/**
	 * @param x X value of chosen item
	 * @param y Y value of chosen item
	 * @return Return the item at given location
	 */
	public Items getItemAt(double x, double y) { //Gets the item at the specified location
		for (Obstacle d : obstacleList){
			if(d instanceof Obstacle){
				if (d.isHere(x, y)) {
					return d;
				}
			}
		}
		for (Drone d : droneList) { //Checks both helicopter and drone list
			for (Helicopter h : helicopterList){
				if (d.isHere(x, y)) {
					if (h.isHere(x, y)) {
						return h;
					}
					return d; 
				}
			}
		}	
		return null;
   }


	/**
	 * @param xVal X value to check if is free
	 * @param yVal Y value to check if is free
	 * @param objects Check all objects if something is there
	 * @return Returns a boolean if it's ok to move there
	 */
	public boolean canMoveHere(double xVal, double yVal, Items objects){ //Checks if the object is going to move out of bounds
		if(xVal < arenaSizeX && xVal > 0 && yVal < arenaSizeY && yVal > 0){
			if(getItemAt(xVal, yVal)== null || getItemAt(xVal, yVal) == objects){
				return true;
			}
		}
		return false;
	}
	
	public String toString() {	//Outputs the arena size
		String output = "Current arena Dimensions: (" +	arenaSizeX + "," + arenaSizeY + ") \t";
	
		for (Items d: droneList){
			output += "\n";
			output += d.toString();
		}
		return output;
	}

	/**
	 * @param m Item to move
	 */
	public void moveAllDrones(CanvasGUI m){ //Moves all objects on the board
		for (Drone d : droneList){
			d.tryToMove(this);
		}
		for(Helicopter H: helicopterList){
			H.tryToMove(this);
			destroy();
		}	
	}	
	
	/**
	 * @param type Type of object to add (assigned by its colour)
	 * @param xPos X position to spawn object
	 * @param yPos Y position to spawn object
	 * @param itemID Unique ID to give spawned object
	 * @param velocity Velocity of object
	 * @param angle Angle of travel to give spawned object
	 */
	public void addItem(char type, double xPos, double yPos, int itemID, double velocity, double angle){
        if (type == 'b'){        //'b' is black, all drones are black
            Drone newEntity = new Drone(xPos,yPos);
            newEntity.setDirection(velocity, angle);
            newEntity.setID(itemID);
            droneList.add(newEntity);
        }
        else if(type == 'g'){        //'g' is for grey obstacles
            Obstacle newEntity = new Obstacle(xPos, yPos);
            newEntity.setID(itemID);
            obstacleList.add(newEntity);
        }
        else if (type == 'r'){     //r is for red helicopters
            Helicopter newEntity = new Helicopter(xPos,yPos);
            newEntity.setDirection(velocity, angle);
            newEntity.setID(itemID);
            helicopterList.add(newEntity);
        }
    }

	public void destroy() {
		for(Helicopter H : helicopterList) { //checks at location of all helicopters
			for(Drone d : droneList) { //check drone at location to see if it intercepts helicopter
				if (H.isTouching(d.getX(), d.getY())) {
					droneList.remove(d);
				}
			}
		}
	}
	
	public String storeArenaData() { //Stores all information about the objects in arena 
		String saveData = "";
		saveData += arenaSizeX + "\n";
		saveData += arenaSizeY;
		for (Drone d : droneList)
		{
		    saveData += "\n";
		    saveData += d.saveDrone();
		}
		for (Helicopter h : helicopterList) 
		{
		    saveData += "\n";
		    saveData += h.saveDrone();
		}
		for (Obstacle o : obstacleList) 
		{
		    saveData += "\n";
		    saveData += o.saveObstacle();
		}
		return saveData;
	}

	public void saveArenaToFile() //Writes the save data to a file
	   {
	        Thread thr = new Thread(new Runnable() {
	            @Override
	            public void run() {
	                JFileChooser select = new JFileChooser(); //Lets user select the file they want
	                int approve = select.showSaveDialog(null);
	                if (approve == JFileChooser.APPROVE_OPTION) 
	                {		
	                    File file = select.getSelectedFile();			
	                    try {
	                        FileWriter writeFile = new FileWriter(file);		
	                        PrintWriter writeHead = new PrintWriter(writeFile);
	                        writeHead.println(storeArenaData()); //Writes to the file		
	                        writeHead.close();
	                    }
	                    catch (FileNotFoundException e) {	//catch any errors that may occur						
	                        e.printStackTrace();
	                    }
	                    catch (IOException e) {									
	                        e.printStackTrace();
	                    }
	                }
	            }
	        });
	        thr.start(); //starts the thread
	 }
	
	/**
	 * @param args Required
	 */
	public static void main(String[] args){
	}
	
}
