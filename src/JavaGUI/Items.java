package JavaGUI;

/**
 * @author Ryan-
 *
 */
public abstract class Items {
    protected double posX;
    protected double posY;
    static int initial = 0;
    int size;
    char type;

    /**
     * @param x X position value of the item
     * @param y Y position value of the item
     */
    public Items(double x, double y) {
        posX = x;
        posY = y;
    }

    /**
     * @return Return the type of item (Drone, Obstacle, Helicopter, etc..)
     */
    public char getType() {
        return type;
    }
    /**
     * @return Returns the X position of the object
     */
    public double getX() {
        return posX;
    }
    /**
     * @return Returns the Y position of the object
     */
    public double getY() {
        return posY;
    }

    /**
     * @return Returns object's size
     */
    public int getSize() {
        return size;
    }

    /**
     * @param xVal X coordinate of where to check for item
     * @param yVal Y coordinate of where to check for item
     * @return Returns true if an item is there
     */
    public boolean isHere(double xVal, double yVal) {//Checks the outermost limits of an item
        double upper = yVal + size; 
        double lower = yVal - size;
        double leftmost = xVal - size;
        double rightmost = xVal + size;

        if ((posX < rightmost && posX > leftmost) && (posY < upper && posY > lower)) {
            return true;
        }
        return false;
    }

    /**
     * @param c Draws the item
     */
    public void drawCircle(CanvasGUI c) { //draws the item
        c.showCircle(posX, posY, size, type);
    }

}