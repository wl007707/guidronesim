package JavaGUI;

//required modules
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.IOException;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.Label;

import javax.swing.*;

/**
 * @author Ryan-
 *
 */
public class DroneInterface extends Application {//initialising the application
    int canvasSize = 500;
    CanvasGUI mc;
    DroneArena arena;
    VBox rPanel;
    boolean animation = false; //Boolean stating whether the drones should move or not

    public void drawInfo() {
        rPanel.getChildren().clear();

        Label l = new Label(arena.toString());
        rPanel.getChildren().add(l);
    }

    public void displaySystem() { //draw the sytem
        mc.clearCanvas();
        arena.showDrone(mc);
        drawInfo();
    }

    /**
     * @param title Title of the text box
     * @param body Text inside the text box
     */
    private void showPopup(String title, String body) { //this is for custom alert messages
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(body);

        alert.showAndWait();
    }


    public void loadFile() { //Handels the loading of a save file
        Thread thr = new Thread(new Runnable() {
            @Override
            public void run() {
                JFileChooser selector = new JFileChooser(); //Opens the file selector
                int validate = selector.showOpenDialog(null);
                if (validate == JFileChooser.APPROVE_OPTION) {// validates the selected file to see if its a proper file type
                    File selectedFile = selector.getSelectedFile();
                    if (selectedFile.isFile()) {//if the selected file is actually a file
                        try {//tries the whole load operation
                            String path = selector.getSelectedFile().getPath(); //gets the file path
                            Path filePath = Paths.get(path);
                            long lineCount = Files.lines(filePath).count(); //finds the number of lines in file
                            double xPosition = 0; //x position of object to be added
                            double yPosition = 0; //y position of object to be added
                            int ID = 0; //object's unique ID
                            char type = 'd'; //what type of object (drone, obstacle, etc..)
                            double velocity = 0; //speed of object
                            double angle = 0; //angle of travel

                            int arenaX = Integer.parseInt(Files.readAllLines(filePath).get(0), 10); //Line 0 states arena's X size
                            int arenaY = Integer.parseInt(Files.readAllLines(filePath).get(1), 10); //Line 1 states arena's Y size
                            arena = new DroneArena(arenaX, arenaY); //Creates new arena based on first 2 lines
                            for (int i = 2; i < lineCount; i = i + 6) { //Each item takes up 6 lines, one for X, one for Y, one for ID, one for Speed, one for Angle, and finally the item type
                                type = Files.readAllLines(filePath).get(i).charAt(0); 
                                ID = Integer.parseInt(Files.readAllLines(filePath).get(i + 1), 10);
                                xPosition = Double.parseDouble(Files.readAllLines(filePath).get(i + 2));
                                yPosition = Double.parseDouble(Files.readAllLines(filePath).get(i + 3));
                                velocity = Double.parseDouble(Files.readAllLines(filePath).get(i + 4));
                                angle = Double.parseDouble(Files.readAllLines(filePath).get(i + 5));
                                arena.addItem(type, xPosition, yPosition, ID, velocity, angle); //Takes all the values, and adds item to canvas
                            }
                            displaySystem(); //Draws the whole system
                        } catch (FileNotFoundException e) { //error handling
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        thr.start();
    }


    /**
     * @return The HBox with all the buttons in
     */
    private HBox functionButtons() {
        Button btnSave = new Button("Save"); //create the save button
        btnSave.setOnAction(new EventHandler < ActionEvent > () {
            public void handle(ActionEvent event) {
                animation = false; //stops the program when saving
                arena.saveArenaToFile(); //save the system
                displaySystem();
            }
        });

        Button btnLoad = new Button("Load"); //load button
        btnLoad.setOnAction(new EventHandler < ActionEvent > () {
            public void handle(ActionEvent event) {
                animation = false; //stops animation when loading
                loadFile(); //load from save file
                displaySystem();
            }
        });

        Button btnInfo = new Button("Info"); //info butotn
        btnInfo.setOnAction(new EventHandler < ActionEvent > () {
            public void handle(ActionEvent event) {
                showInfo(); //displays pop up window
                displaySystem();
            }
        });

        Button btnQuit = new Button("Quit"); //quit button
        btnQuit.setOnAction(new EventHandler < ActionEvent > () {
            public void handle(ActionEvent event) {
                System.exit(0); //closes app
            }
        });

        return new HBox(btnSave, btnLoad, btnInfo, btnQuit); //Horizontal box with all the buttons in
    }

    /**
     * @return The HBox with all the buttons in
     */
    private HBox utilityButtons() {
        Button btnToggle = new Button("Start/Stop"); //Start/stop button
        btnToggle.setOnAction(new EventHandler < ActionEvent > () {
            public void handle(ActionEvent event) {//toggle animation
                if (animation == true) {
                    animation = false;
                } else {
                    animation = true;
                }
            }
        });
        Button btnAddD = new Button("Add Drone");//add drone button
        btnAddD.setOnAction(new EventHandler < ActionEvent > () {
            public void handle(ActionEvent event) {
                arena.addDrone('d');//add drone
                displaySystem();
            }
        });
        Button btnAddO = new Button("Add Obstacle"); //add obstacle button
        btnAddO.setOnAction(new EventHandler < ActionEvent > () {
            public void handle(ActionEvent event) {
                arena.addDrone('o'); //adds the obstacle
                displaySystem();
            }
        });
        Button btnAddH = new Button("Add Helicopter"); //add helicopter button
        btnAddH.setOnAction(new EventHandler < ActionEvent > () {

            public void handle(ActionEvent event) {
                arena.addDrone('H');//add the helicopter
                displaySystem();
            }
        });

        return new HBox(btnToggle, btnAddD, btnAddO, btnAddH); //Horizontal box with all the buttons in
    }

    public void showInfo() {
        showPopup("Info", "Drone simulation with graphical user interface by Ryan Baldwin"); //Info text about my project
    }

    public void start(Stage primaryStage) throws Exception { //starting up the program
        primaryStage.setTitle("Drone Simulation");
        BorderPane bp = new BorderPane();
        bp.setTop(functionButtons());
        bp.setBottom(utilityButtons());
        Group root = new Group();
        Canvas canvas = new Canvas(canvasSize, canvasSize); //initialises canvas

        root.getChildren().add(canvas);

        mc = new CanvasGUI(canvas, canvasSize, canvasSize); //creates canvas
        arena = new DroneArena(canvasSize, canvasSize);
        bp.setCenter(root);
        rPanel = new VBox();
        bp.setRight(rPanel); //adds info panel to gui

        Scene scene = new Scene(bp, canvasSize * 1.6, canvasSize * 1.2);

        new AnimationTimer() { //Runs the program at a fixed rate
            public void handle(long currentNanoTime) {
                if (animation) {
                    arena.moveAllDrones(mc);
                    displaySystem();
                }
            }
        }.start();

        primaryStage.setScene(scene); //sets the scene
        primaryStage.show();
    }

    /**
     * @param args Required
     */
    public static void main(String[] args) {
        Application.launch(args);
    }
}