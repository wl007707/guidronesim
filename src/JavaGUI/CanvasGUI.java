package JavaGUI;

//Required modules
import javafx.application.Application;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.geometry.VPos;


/**
 * @author Ryan-
 *
 */
public class CanvasGUI extends Application {

    int canvasX = 500; //Canvas size X
    int canvasY = 500;//Canvas size Y
    GraphicsContext gc;

    
    /**
     * @param a The canvas to create
     * @param x Size of canvas X
     * @param y Size of canvas Y
     */
    public CanvasGUI(Canvas a, int x, int y) {
        gc = a.getGraphicsContext2D(); //Initialised 2D canvas
        canvasX = x;
        canvasY = y;
    }

    /**
     * @return Canvas size X
     */
    public int getCanvasX() {
        return canvasX;
    }
    /**
     * @return Canvas size Y
     */
    public int getCanvasY() {
        return canvasY;
    }

    /**
     * @param width width of the canvas
     * @param height height of the canvas
     */
    public void fillCanvas(int width, int height) {
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, width, height);
        gc.setStroke(Color.BLACK);
        gc.strokeRect(0, 0, width, height);
    }

    /**
     * @param col Desired colour
     * @return Returns the colour based on what the object is going to be
     */
    Color changeFillColour(char col) {
        Color fillCol = Color.WHITE;
        switch (col) {
            case 'r':
            case 'R':
                fillCol = Color.RED;
                break;
            case 'b':
            case 'B':
                fillCol = Color.BLACK;
                break;
            case 'g':
            case 'G':
                fillCol = Color.GRAY;
                break;
        }
        return fillCol;
    }

    /**
     * @param c Desired colour
     */
    public void setFillColour(Color c) {
        gc.setFill(c);
    }

    /**
     * @param x X value for circle
     * @param y Y value for circle
     * @param rad Radius of circle
     * @param colour Colour of circle
     */
    public void showCircle(double x, double y, double rad, char colour) {
        setFillColour(changeFillColour(colour));
        showCircle(x, y, rad);
    }

    /**
     * @param x X value for circle
     * @param y Y value for circle
     * @param rad Radisu of circle
     */
    public void showCircle(double x, double y, double rad) {
        gc.fillArc(x - rad, y - rad, rad * 2, rad * 2, 0, 360, ArcType.ROUND);

    }

    /**
     * @param x X position of text
     * @param y Y position of Text
     * @param s Text to be shown
     */
    public void showText(double x, double y, String s) {
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setTextBaseline(VPos.CENTER);
        gc.setFill(Color.WHITE);
        gc.fillText(s, x, y);
    }

    public void clearCanvas() {
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, canvasX, canvasY);
    }

    public void start(Stage arg0) throws Exception {}

}