package JavaGUI;

/**
 * @author Ryan-
 *
 */
public class Obstacle extends Items {
    int obstacleID;
    int velocity = 0; //Obstacles are items that do not move
    int angle = 0; //hence dont need a travel angle

    /**
     * @param x X position of obstacle
     * @param y Y position of obstacle
     */
    public Obstacle(double x, double y) {
        super(x, y);
        size = 5;
        type = 'g';
    }

    public String toString() {
        return null;
    }

    /**
     * @param a Obstacle inherits from Item, so never tries to move
     */
    public void tryToMove(DroneArena a) {}

    /**
     * @param ID  Unique ID of obstacle
     */
    public void setID(int ID) {
        obstacleID = ID;
    }

    /**
     * @return Formatted string of information to save about the obstacles
     */
    public String saveObstacle() {
        String saveData = "";
        saveData += type + "\n"; //Type of item stored by their colour. This makes it easier to draw later when loading
        saveData += obstacleID + "\n"; 
        saveData += posX + "\n";
        saveData += posY + "\n";

        saveData += velocity + "\n"; //velocity and angle set to 0. this is still included because it makes file loading easier since all object take up 6 lines for storage
        saveData += angle;
        return saveData;
    }

}